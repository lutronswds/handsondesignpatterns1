﻿using System;

namespace DependencyInjectionExercise
{

    class Program
    {

        static void Main(string[] args)
        {

            NetworkSender networkSender = new NetworkSender();

            INetworkCommunicator networkCommunicator = new ProductionNetworkCommunicator();
            //INetworkCommunicator networkCommunicator = new MockNetworkCommunicator();

            networkSender.NetworkCommunicator = networkCommunicator;

            string dataToSend = "Hello World";

            Console.Write("Sent \"" + dataToSend + "\"");

            string response = networkSender.FormatAndSendMessage(dataToSend);

            Console.WriteLine("\" received \"" + response + "\"");

            Console.WriteLine();
            Console.WriteLine("Press <Enter> to end.");
            Console.ReadLine();

        }

    }

}
