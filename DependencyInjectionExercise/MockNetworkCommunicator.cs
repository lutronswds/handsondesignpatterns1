﻿namespace DependencyInjectionExercise
{

    public class MockNetworkCommunicator : INetworkCommunicator
    {

        bool sendNetworkMessageAndReceiveResponseCalled;
        string networkMessageToSendPassedIn;

        public MockNetworkCommunicator()
        {
            // Intentionally empty
        }

        public string SendNetworkMessageAndReceiveResponse(string networkMessageToSend)
        {
            sendNetworkMessageAndReceiveResponseCalled = true;
            networkMessageToSendPassedIn = networkMessageToSend;

            return networkMessageToSend;
        }

        public bool WasSendNetworkMessageAndReceiveResponseCalled()
        {
            return sendNetworkMessageAndReceiveResponseCalled;
        }

        public string GetNetworkMessageToSendPassedIn()
        {
            return networkMessageToSendPassedIn;
        }

    }

}
