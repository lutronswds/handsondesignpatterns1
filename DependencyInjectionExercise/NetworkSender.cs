﻿using System;

namespace DependencyInjectionExercise
{

    // THIS IS THE CLASS YOU ARE TESTING DO NOT CHANGE THIS CLASS!!!
    public class NetworkSender
    {

        const string errorPreamble = "<!!!!!!!!>";
        const string preamble = "<PREAMBLE>";
        const string postamble = "<POSTAMBLE>";

        public NetworkSender()
        {
            // Intentionally empty
        }

        public NetworkSender(INetworkCommunicator networkCommunicator)
        {
            NetworkCommunicator = networkCommunicator;
        }

        public INetworkCommunicator NetworkCommunicator { get; set; }

        public string FormatAndSendMessage(string dataToSend)
        {
            string networkMessageToSend = PrepareNetworkMessageForSending(dataToSend);

            string networkResponseReceived = NetworkCommunicator.SendNetworkMessageAndReceiveResponse(networkMessageToSend);

            return ProcessReceivedNetworkResponse(networkResponseReceived);
        }

        private string PrepareNetworkMessageForSending(string dataToSend)
        {
            return preamble + dataToSend + postamble;
        }

        private string ProcessReceivedNetworkResponse(string receivedNetworkResponse)
        {
            if (receivedNetworkResponse.StartsWith(errorPreamble))
            {
                throw new ServerErrorExeception(receivedNetworkResponse.Substring(errorPreamble.Length,
                                                                                  receivedNetworkResponse.Length - errorPreamble.Length - postamble.Length));
            }
            else if (!receivedNetworkResponse.StartsWith(preamble) || !receivedNetworkResponse.EndsWith(postamble))
            {
                throw new BadDataExpection(receivedNetworkResponse);
            }
            else
            {
                return receivedNetworkResponse.Substring(preamble.Length,
                                                         receivedNetworkResponse.Length - preamble.Length - postamble.Length);
            }
        }

    }

    public class ServerErrorExeception : Exception 
    {
        public ServerErrorExeception(string serverErrorData)
            : base(serverErrorData)
        {
            // Intentionally empty
        }
    }

    public class BadDataExpection : Exception
    {
        public BadDataExpection(string data)
            : base(data)
        {
            // Intentionally empty
        }
    }


}
