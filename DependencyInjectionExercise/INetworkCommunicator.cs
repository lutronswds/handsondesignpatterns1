﻿namespace DependencyInjectionExercise
{

    public interface INetworkCommunicator
    {

        string SendNetworkMessageAndReceiveResponse(string networkMessageToSend);

    }

}
