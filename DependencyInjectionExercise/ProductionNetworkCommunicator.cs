﻿using System.Linq;
using System.Threading;

namespace DependencyInjectionExercise
{

    // DO NOT CHANGE THIS CLASS, CHANGE MockNetworkCommunicator INSTEAD
    public class ProductionNetworkCommunicator : INetworkCommunicator
    {
        const string preamble = "<PREAMBLE>";
        const string postamble = "<POSTAMBLE>";

        public ProductionNetworkCommunicator()
        {
            // Intentionally empty
        }

        public string SendNetworkMessageAndReceiveResponse(string networkMessageToSend)
        {
            Thread.Sleep(30 * 1000);

            return preamble +
                   new string(networkMessageToSend.Substring(preamble.Length,
                                                             networkMessageToSend.Length - preamble.Length - postamble.Length).Reverse().ToArray()) +
                   postamble;
        }

    }

}
